
randomColors = ["1abc9c", "2ecc71", "3498db", "9b59b6", "34495e", "f1c40f ", "e67e22", "e74c3c", "7f8c8d", "eb3b5a"];
var smsHistory = TAFFY();
data = '';

function giveRandomColor() {
    randomNumber = Math.floor(Math.random() * 10);
    let randomColor = randomColors[randomNumber];
    return randomColor;3
}

function giveCurrentDateTime() {   
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var hh = today.getHours();
    var mi = today.getMinutes();
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (hh < 10) {
        hh = '0' + hh
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    if (mi < 10) {
        mi = '0' + mi
    }
    today = dd + '/' + mm + '/' + yyyy + ' | ' + hh + ':' + mi;
    return today;
}

function giveRandomSix() {
    return Math.floor(100000 + Math.random() * 900000);
}

function openContactCard(k) {
    // var fruitCount = this.getAttribute('data-index');
    // alert(k);
    let name = data[k].Name;
    let number = data[k].Number;
    document.getElementById('popOverButton').setAttribute('data-person',k);
    document.getElementById('popOverName').innerHTML = name;
    document.getElementById('popOverNumber').innerHTML = '+91 '+number;
    document.getElementById('overlay').style.display = 'block';
    document.getElementById('popOver').style.display = 'block';
}

var tab1 = document.getElementById("tab1");
tab1.addEventListener("click", function (e) {
    document.getElementById('tab1').classList.add("selected");
    document.getElementById('tab2').classList.remove("selected");
    document.getElementById('contacts').style.display = 'block';
    document.getElementById('smsHistory').style.display = 'none';
}, false);

var tab2 = document.getElementById("tab2");
tab2.addEventListener("click", function (e) {
    document.getElementById('tab2').classList.add("selected");
    document.getElementById('tab1').classList.remove("selected");
    document.getElementById('contacts').style.display = 'none';
    document.getElementById('smsHistory').style.display = 'block';

    var allHistory = smsHistory().get();
    document.getElementById('smsHistory').innerHTML = '&emsp;Total SMS sent : '+allHistory.length+'<br>';

    if(allHistory.length > 0){

        for (let i = allHistory.length;i--;){
            element = allHistory[i];
            // console.log(i);
            const otp = decodeURI(element.otp);
            const name = decodeURI(element.name);
            const char = name[0];
            const time = element.time;
            const boiler = `<div class="contact historyContact">
                <span class="historyname">${name}</span><br>
                <span class="smsStamp"><b>OTP: ${otp}</b>&emsp; sent at ${time}</span>
            </div>`;
            // console.log(boiler);
            document.getElementById('smsHistory').innerHTML += boiler;

        }

    }

}, false);



var overlay = document.getElementById("overlay");
overlay.addEventListener("click", function (e) {
    document.getElementById('overlay').style.display = 'none';
    document.getElementById('popOver').style.display = 'none';
    document.getElementById('notification').style.display = 'none';
}, false);


var popOverButton = document.getElementById("popOverButton");
popOverButton.addEventListener("click", function (e) {
    
    let person = document.getElementById('popOverButton').getAttribute('data-person');
    document.getElementById('sendbutton').setAttribute('data-person', person);
    document.getElementById('textArea').value = 'Hi.  Your  OTP  is: '+giveRandomSix();

    // alert(person);

    let personObj = {person:person};

    let personHistory = smsHistory(personObj).get();
    // console.log(personHistory);

    if(personHistory.length > 0){
        document.getElementById('personSmsHistory').innerHTML = '';


        // alert('inside');

        for (let i = 0; i < personHistory.length; ++i) {
            element = personHistory[i];
            // console.log(i);
            const sms = decodeURI(element.sms);
            const time = element.time;
            const boiler = `<div class="singleSms">
                <div class="smsText">${sms}</div>
                <div class="smsStamp">${time}</div>
            </div>`;
            // console.log(boiler);
            document.getElementById('personSmsHistory').innerHTML += boiler;
            
        }
    }else{
        
        const boiler = `<div class="singleSms">
            <div class="smsText">no sms history for this contact</div>
        </div>`;
        // console.log(boiler);
        document.getElementById('personSmsHistory').innerHTML = boiler;
    }


    let name = data[person].Name;
    let number = data[person].Number;

    document.getElementById('titleName').innerHTML = name;
    document.getElementById('titleNumber').innerHTML = '+91 ' + number;
    document.getElementById('container').style.display = 'none';
    document.getElementById('smsScreen').style.display = 'block';
}, false);

var sendbutton = document.getElementById("sendbutton");
sendbutton.addEventListener("click", function (e) {

    document.getElementById('titleNumber').innerHTML = 'Sending SMS . . .';
    let person = document.getElementById('sendbutton').getAttribute('data-person');

    let name = data[person].Name;
    let number = data[person].Number;
    let sms = encodeURI(document.getElementById('textArea').value);
    if(sms == ''){
        alert('Textarea is Empty!');
        return;
    }
    let otp = sms.substr(sms.length - 6);
    let currentDateTime = giveCurrentDateTime();
    let isSent = sendSMS(person,name,number,sms,currentDateTime,otp);
    if(isSent){
        
    }
}, false);



var closeSms = document.getElementById('closeSms');
closeSms.addEventListener('click',function (e) {

    document.getElementById('smsScreen').style.display = 'none';
    document.getElementById('container').style.display = 'block';

},false);



function loadDoc() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let placeholder = document.getElementById('loadingContacts');
            placeholder.parentNode.removeChild(placeholder);
            data = JSON.parse(this.responseText);

            for (let i = 0; i < data.length; ++i) {
                element = data[i];
                const fullName = element.Name;
                const avatarLetter = fullName[0];
                const avatarBg = giveRandomColor();
                const boiler = `<div class="contact" data-index="${i}" onClick="openContactCard(${i});">
                    <span class="avatar" style="background:#${avatarBg}">${avatarLetter}</span>
                    <span class="fullName">${fullName}</span>
                </div>`;
                document.getElementById("contacts").innerHTML += boiler;
                
            }
        }
    };
    xhttp.open("GET", "contacts.csv", true);
    xhttp.send();
}

function sendSMS(person, name, number, sms, currentDateTime, otp) {
    
    var data = 'To=%2B91' + number + '&Body=' + sms +'&From=+14105931249';
    var xhr = new XMLHttpRequest();
    // xhr.withCredentials = true;
    xhr.onreadystatechange = function () {
        responseIs = JSON.parse(this.responseText);
        if (this.readyState == 4 && this.status === 201) {
            // console.log(this.responseText);
            let wholeSms = {
                person: person,
                name: name,
                number: number,
                sms: sms,
                time: currentDateTime,
                otp: otp
            }
            smsHistory.insert(wholeSms);
            document.getElementById('textArea').value = '';
            document.getElementById('titleNumber').innerHTML = '+91 ' + number;
            
            let personObj = { person: person };
            let personHistory = smsHistory(personObj).get();
            document.getElementById('personSmsHistory').innerHTML = '';
            for (let i = 0; i < personHistory.length; ++i) {
                element = personHistory[i];
                // console.log(i);
                const sms = decodeURI(element.sms);
                const time = element.time;
                const boiler = `<div class="singleSms">
                <div class="smsText">${sms}</div>
                <div class="smsStamp">${time}</div>
                </div>`;
                // console.log(boiler);
                document.getElementById('personSmsHistory').innerHTML += boiler;
                
            }
            return true;
        } else if (this.readyState == 4 && this.status == 400) {
            console.log(this.responseText);
            alert (responseIs.message);
            document.getElementById('titleNumber').innerHTML = '+91 ' + number;
            return false
        }
        
    };
    xhr.open("POST", "https://api.twilio.com/2010-04-01/Accounts/AC90426c4286f1b0910995acc458dcd8e9/Messages.json");
    xhr.setRequestHeader("content-type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("authorization", "Basic QUM5MDQyNmM0Mjg2ZjFiMDkxMDk5NWFjYzQ1OGRjZDhlOTo3MDFjZTVhNWJhYTYxN2E1ZjU0ZmE2MTEyODk3YmJkOA==");
    // xhr.setRequestHeader("cache-control", "no-cache");
    // xhr.setRequestHeader("Access-Control-Allow-Origin", 'http://127.0.0.1');
    // xhr.setRequestHeader("Access-Control-Allow-Credentials", true);
    xhr.send(data);

}







window.onload = function () {
    loadDoc();
};